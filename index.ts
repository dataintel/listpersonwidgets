import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPersonComponent } from './src/listperson.component';

export * from './src/listperson.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ListPersonComponent
  ],
  exports: [
    ListPersonComponent
  ]
})
export class ListPersonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ListPersonModule,
      providers: []
    };
  }
}
