import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListPersonModule } from '../../../index';
import { AppComponent } from './app.component';
import { PersonvertivalComponent } from './personvertival/personvertival.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonvertivalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ListPersonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
