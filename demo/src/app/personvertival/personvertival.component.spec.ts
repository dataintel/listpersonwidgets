import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonvertivalComponent } from './personvertival.component';

describe('PersonvertivalComponent', () => {
  let component: PersonvertivalComponent;
  let fixture: ComponentFixture<PersonvertivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonvertivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonvertivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
