import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personvertival',
  templateUrl: './personvertival.component.html',
  styleUrls: ['./personvertival.component.css']
})
export class PersonvertivalComponent implements OnInit {

public listperson = [
      {
        "count": 1858,
        "id": "najib+razak",
        "name": "Najib Razak",
        "image": "/assets/nouser.png"
      },
      {
        "count": 392,
        "id": "mahathir+mohamad",
        "name": "Mahathir Mohamad",
        "image": "/assets/nouser.png"
      },
      {
        "count": 195,
        "id": "kim+jong+nam",
        "name": "Kim Jong Nam",
        "image": "/assets/nouser.png"
      },
      {
        "count": 169,
        "id": "najib",
        "name": "Najib",
        "image": "/assets/nouser.png"
      },
      {
        "count": 147,
        "id": "kang+chol",
        "name": "Kang Chol",
        "image": "/assets/nouser.png"
      },
      {
        "count": 117,
        "id": "ahmad+zahid+hamidi",
        "name": "Ahmad Zahid Hamidi",
        "image": "/assets/nouser.png"
      },
      {
        "count": 109,
        "id": "lim+kit+siang",
        "name": "Lim Kit Siang",
        "image": "/assets/nouser.png"
      },
      {
        "count": 105,
        "id": "kim+jong+un",
        "name": "Kim Jong Un",
        "image": "/assets/nouser.png"
      },
      {
        "count": 83,
        "id": "sultan+muhammad+v",
        "name": "Sultan Muhammad V",
        "image": "/assets/nouser.png"
      },
      {
        "count": 83,
        "id": "abdul+hadi+awang",
        "name": "Abdul Hadi Awang",
        "image": "/assets/nouser.png"
      }
    ];
  constructor() { }

  ngOnInit() {
  }

}
